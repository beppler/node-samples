var fs = require('fs'),
    http = require('http'),
    soap = require('soap');

var wsdl = fs.readFileSync(__dirname + '/echo.wsdl', 'utf8'),
    services = {
        EchoServer: {
            EchoPort: {
                Echo: function(args) {
                    return args.message;
                }
            }
        }
    };

var server = http.createServer(function(request,response) {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end(http.STATUS_CODES[404]+": "+request.url)
    }),
    soapServer = soap.listen(server, '/echo', services, wsdl);

soapServer.log = function (type, data) { console.log(type + ": " + data); }

server.listen(8080, 'localhost');
