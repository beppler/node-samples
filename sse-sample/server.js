var http = require("http");
var url = require("url");
var fs = require("fs");
http.createServer(function(request, response) {
    var parsedURL = url.parse(request.url, true);
    var pathname = parsedURL.pathname;
    var args = pathname.split("/");
    var method = args[1];
    if (method === "") {
        response.writeHead(200, {
            "Content-Type": "text/html",
            "Cache-Control": "no-cache",
            "Connection": "close"
        });
        var stream = fs.createReadStream(__dirname + "/client.html");
        stream.pipe(response);
        console.log("sending client html");
    }
    else if (method === "sse") {
        response.writeHead(200, {
            "Content-Type": "text/event-stream",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive"
        });
        response.write(":" + Array(2049).join(" ") + "\n"); 
        response.write("retry: 2000\n");
        response.on("close", function() {
            console.log("client disconnected");
        });
        setInterval(function() {
            response.write("data: " + new Date() + "\n\n");
        }, 1000);
        console.log("sending sse events");
    }
}).listen(8080, "localhost");
console.log("listening on port 8080");