var forge = require('node-forge');

console.log('Generating 2048-bit key-pair...');
var keys = forge.pki.rsa.generateKeyPair(2048);
console.log('Key-pair created.');

console.log('Creating self-signed certificate...');
var cert = forge.pki.createCertificate();
cert.publicKey = keys.publicKey;
cert.serialNumber = (new Date()).getTime().toString();
cert.validity.notBefore = new Date();
cert.validity.notAfter = new Date();
cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 10);
var attrs = [
    { 'name': 'commonName', 'value': 'example.org' },
    { 'name': 'organizationName', 'value': 'Example' },
    { 'name': 'localityName', 'value': 'Curitiba' },
    { 'name': 'stateOrProvinceName', 'value': 'Parana' },
    { 'name': 'countryName', 'value': 'BR' },
];
cert.setSubject(attrs);
cert.setIssuer(attrs);
cert.setExtensions([
    { 'name': 'basicConstraints', 'cA': true },
    { 'name': 'keyUsage', 'keyCertSign': true, 'digitalSignature': true, 'nonRepudiation': true, 'keyEncipherment': true, 'dataEncipherment': true },
    { 'name': 'extKeyUsage', 'serverAuth': true, 'clientAuth': true, 'codeSigning': true, 'emailProtection': true, 'timeStamping': true },
    { 'name': 'nsCertType', 'client': true, 'server': true, 'email': true, 'objsign': true, 'sslCA': true, 'emailCA': true, 'objCA': true },
    { 'name': 'subjectAltName', 'altNames' : [ { 'type': 6, 'value': 'http://example.org/webid#me' }, { 'type': 7, 'value': '127.0.0.1' } ] },
    { 'name': 'subjectKeyIdentifier' }
]);

// self-sign certificate
cert.sign(keys.privateKey, forge.md.sha256.create());
console.log('Certificate created.');

// PEM-format keys and cert
var pem = {
  privateKey: forge.pki.privateKeyToPem(keys.privateKey),
  publicKey: forge.pki.publicKeyToPem(keys.publicKey),
  certificate: forge.pki.certificateToPem(cert)
};

console.log('\nKey-Pair:');
console.log(pem.privateKey);
console.log(pem.publicKey);

console.log('\nCertificate:');
console.log(pem.certificate);
