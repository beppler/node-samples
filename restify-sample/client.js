var restify = require('restify');

var client = restify.createJsonClient({
  url: 'http://localhost:8080',
  version: '~1.0'
});

client.post('/users', { name: "John Doe" }, function (err, req, res, obj) {
  if(err) {
  	console.log("An error ocurred:", err);
  	process.exit();
  }
  console.log('POST    /users   returned: %j', obj);
  var id = obj.id;
  
  client.get('/users/' + id, function (err, req, res, obj) {
    if(err) console.log("An error ocurred:", err);
    else console.log('GET     /users/' + id + ' returned: %j', obj);
    
    client.put('/users/' + id, { country: "USA" }, function (err, req, res, obj) {
      if(err) console.log("An error ocurred:", err);
      else console.log('PUT     /users/' + id + ' returned: %j', obj);
      
      client.del('/users/' + id, function (err, req, res, obj) {
        if(err) console.log("An error ocurred:", err);
        else console.log('DELETE  /users/' + id + ' returned: %j', obj);
        
        client.get('/users', function (err, req, res, obj) {
          if(err) console.log("An error ocurred:", err);
          else console.log('GET     /users       returned: %j', obj);

          process.exit();
        });
      });
    });
  });
});