var connect = require('connect'),
    connectRoute = require('connect-route'),
    http = require('http');

var app = connect()
    .use(connect.favicon())
    .use(connect.logger('dev'))
    .use(connect.static('public'))
    .use(connect.directory('public'))
    .use(connect.cookieParser())
    .use(connect.session({ secret: 'my secret here' }))
    .use(connectRoute(function (router) {
        router.get('/', function (req, res) {
          res.end('Hello from Connect!\n');  
        });
    }));

var server = http.createServer(app);
server.listen(8080, 'localhost', function () {
    console.log('listening at http://%s:%d/', server.address().address, server.address().port);
});